class Color:
    wall = (192, 192, 192)
    block = (124, 148, 97)
    player = (244, 175, 245)
    black = (0, 0, 0)
    white = (255, 255, 255)
    background = (240, 234, 227)
    goal = (0, 255, 0)
