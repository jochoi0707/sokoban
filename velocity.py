class Velocity:
    """
    This class should express a sprite's
    velocity in 2D-space
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y
