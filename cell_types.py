from enum import Enum


class Cell(Enum):
    """
    This class should designate the
    entity contained in any particular
    grid cell
    """

    empty = 1
    player = 2
    block = 3
    wall = 4
