def reversible_inclusive_list(start, finish):
    return (
        list(range(start, finish + 1))
        if start < finish
        else list(range(start, finish - 1, -1))
    )


ans = reversible_inclusive_list(1, 5)
exp = [1, 2, 3, 4, 5]
assert exp == ans, f"expected {exp} got {ans}"
ans = reversible_inclusive_list(2, 8)
exp = [2, 3, 4, 5, 6, 7, 8]
assert exp == ans, f"expected {exp} got {ans}"
ans = reversible_inclusive_list(10, 20)
exp = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
assert exp == ans, f"expected {exp} got {ans}"
ans = reversible_inclusive_list(24, 17)
exp = [24, 23, 22, 21, 20, 19, 18, 17]
assert exp == ans, f"expected {exp} got {ans}"

print("Everything is gonna be okay")
