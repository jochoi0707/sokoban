import pygame
from constants import (
    SCREEN_SIDE,
    SCREEN_SIZE,
    FRAMERATE,
    BLOCK_SIDE,
    MOVES_POSITION,
    FONT_SIZE,
)
from game_state import GameState
from colors import Color
from directions import Dir
from cell_types import Cell
from player import Player
from block import Block
from wall import Wall
from goal import Goal


class Game:
    def __init__(self):
        pygame.init()
        self.win = pygame.display.set_mode(SCREEN_SIZE)
        pygame.display.set_caption("Sokoban")
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, FONT_SIZE)

    def run(self):
        self.restart()
        while self.state in (GameState.play, GameState.win):
            self.clock.tick(FRAMERATE)
            if self.state == GameState.win:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.state = GameState.quit
                    elif event.type == pygame.KEYDOWN:
                        key = event.key
                        if key == pygame.K_q:
                            self.state = GameState.quit
                        elif key == pygame.K_r:
                            self.restart()

                self.win.fill(Color.background)

                text = self.font.render("You win!", True, Color.black)
                self.win.blit(text, (100, 100))

                text = self.font.render(
                    "Press 'q' to quit the game", False, Color.black
                )
                self.win.blit(text, (100, 200))

                text = self.font.render(
                    "Press 'r' to restart the game", True, Color.black
                )
                self.win.blit(text, (100, 300))

            elif self.state == GameState.play:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.state = GameState.quit
                    elif event.type == pygame.KEYDOWN:
                        key = event.key
                        if key == pygame.K_LEFT:
                            self.player.move_left()
                        elif key == pygame.K_RIGHT:
                            self.player.move_right()
                        elif key == pygame.K_UP:
                            self.player.move_up()
                        elif key == pygame.K_DOWN:
                            self.player.move_down()
                        elif key == pygame.K_q:
                            self.state = GameState.quit

                block_collisions = pygame.sprite.spritecollide(
                    self.player, self.blocks, False
                )
                for block in block_collisions:
                    if self.player.dir == Dir.left:
                        block.move_left()
                    elif self.player.dir == Dir.right:
                        block.move_right()
                    elif self.player.dir == Dir.up:
                        block.move_up()
                    elif self.player.dir == Dir.down:
                        block.move_down()

                count = 0
                for block in self.blocks:
                    pos = (block.rect.x, block.rect.y)
                    if pos in self.goal_positions:
                        count += 1
                if count == len(self.goal_positions):
                    print("you win!")
                    self.state = GameState.win

                self.sprites.update()
                self.draw(self.sprites)

            pygame.display.flip()

        pygame.quit()

    def draw(self, sprites):
        self.win.fill(Color.background)
        sprites.draw(self.win)

        # place the score drawing code here
        text = self.font.render(f"Moves: {self.player.moves}", True, Color.black)
        self.win.blit(text, (10, 10))

    def restart(self):
        self.state = GameState.play
        grid = {}
        for row in range(0, SCREEN_SIDE, BLOCK_SIDE):
            for col in range(0, SCREEN_SIDE, BLOCK_SIDE):
                grid[(row, col)] = Cell.empty

        player_pos = (300, 100)
        player = Player(player_pos, grid)
        self.player = player

        block_pos = (400, 300)
        block1 = Block(block_pos, grid)

        block_pos = (200, 200)
        block2 = Block(block_pos, grid)

        block_pos = (100, 400)
        block3 = Block(block_pos, grid)

        wall1 = Wall((0, 0), grid)
        wall2 = Wall((300, 0), grid)
        wall3 = Wall((300, 300), grid)
        wall4 = Wall((300, 400), grid)

        sprites = pygame.sprite.Group()
        self.sprites = sprites
        blocks = pygame.sprite.Group()
        self.blocks = blocks

        goal_positions = (
            (200, 100),
            (500, 100),
            (300, 200),
        )
        self.goal_positions = goal_positions
        for position in goal_positions:
            goal = Goal(position, grid)
            sprites.add(goal)

        sprites.add(wall1)
        sprites.add(wall2)
        sprites.add(wall3)
        sprites.add(wall4)

        sprites.add(player)

        sprites.add(block1)
        sprites.add(block2)
        sprites.add(block3)

        blocks.add(block1)
        blocks.add(block2)
        blocks.add(block3)


if __name__ == "__main__":
    game = Game()
    game.run()
