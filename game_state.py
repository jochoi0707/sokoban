from enum import Enum


class GameState(Enum):
    play = 1
    win = 2
    lose = 3
    quit = 4
