import pygame
from colors import Color
from constants import (
    BLOCK_SIDE,
    BLOCK_SPEED,
    SCREEN_SIDE,
    SCREEN_LIMIT,
)
from velocity import Velocity
from directions import Dir
from cell_types import Cell


class Block(pygame.sprite.Sprite):
    def update(self):
        if not self.next_pos:
            return

        self.rect.x += self.velocity.x
        self.rect.y += self.velocity.y

        left, top = self.next_pos

        if (self.velocity.x < 0 and self.rect.x < left) or (
            self.velocity.x > 0 and self.rect.x > left
        ):
            self.velocity.x = 0
            self.rect.x = left

        if (self.velocity.y < 0 and self.rect.y < top) or (
            self.velocity.y > 0 and self.rect.y > top
        ):
            self.velocity.y = 0
            self.rect.y = top

    def move_left(self):
        self.next_pos = (self.rect.x - BLOCK_SIDE, self.rect.y)
        self.rect.x -= 10
        self.velocity.x = -BLOCK_SPEED
        self.grid[self.next_pos] = Cell.block

    def move_right(self):
        self.next_pos = (self.rect.x + BLOCK_SIDE, self.rect.y)
        self.rect.x += 10
        self.velocity.x = BLOCK_SPEED
        self.grid[self.next_pos] = Cell.block

    def move_up(self):
        self.next_pos = (self.rect.x, self.rect.y - BLOCK_SIDE)
        self.rect.y -= 10
        self.velocity.y = -BLOCK_SPEED
        self.grid[self.next_pos] = Cell.block

    def move_down(self):
        self.next_pos = (self.rect.x, self.rect.y + BLOCK_SIDE)
        self.rect.y += 10
        self.velocity.y = BLOCK_SPEED
        self.grid[self.next_pos] = Cell.block

    def __init__(self, pos, grid):
        super().__init__()
        self.grid = grid
        self.grid[pos] = Cell.block
        self.dir = None
        self.next_pos = None
        self.side = BLOCK_SIDE

        dim = (self.side, self.side)

        self.image = pygame.Surface(dim)

        self.image.fill(Color.black)
        self.image.set_colorkey(Color.black)

        rect = (0, 0, self.side, self.side)
        pygame.draw.rect(self.image, Color.block, rect)

        self.rect = self.image.get_rect()

        self.rect.x, self.rect.y = pos

        self.velocity = Velocity(0, 0)
