from enum import Enum


class Dir(Enum):
    up = 1
    down = 2
    left = 3
    right = 4
