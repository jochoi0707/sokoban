from collections import namedtuple

Point = namedtuple("Point", ["x", "y"])

p1 = Point(100, 500)

p1.x += 10

print(p1.x, p1.y)
