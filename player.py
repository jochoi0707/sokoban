import pygame
from colors import Color
from directions import Dir
from constants import (
    SCREEN_SIDE,
    SCREEN_LIMIT,
    BLOCK_SIDE,
    PLAYER_SPEED,
)
from velocity import Velocity
from cell_types import Cell


class Player(pygame.sprite.Sprite):
    def update(self):
        if not self.next_pos:
            return

        self.rect.x += self.velocity.x
        self.rect.y += self.velocity.y

        left, top = self.next_pos

        if (self.velocity.x < 0 and self.rect.x < left) or (
            self.velocity.x > 0 and self.rect.x > left
        ):
            self.dir = None
            self.velocity.x = 0
            self.rect.x = left

        if (self.velocity.y < 0 and self.rect.y < top) or (
            self.velocity.y > 0 and self.rect.y > top
        ):
            self.dir = None
            self.velocity.y = 0
            self.rect.y = top

    def move_up(self):
        if self.dir:
            return

        next_y = self.rect.y - BLOCK_SIDE
        if next_y < 0:
            return

        # check if the destination of the player is already occupied by a block
        next_pos = (self.rect.x, next_y)
        if self.grid.get(next_pos) == Cell.block:
            # if so, check the next next position
            next_next_pos = (self.rect.x, next_y - BLOCK_SIDE)
            # is the next next position still in the game screen
            if next_next_pos[1] < 0:
                return
            # make sure that that position is empty (Cell.empty)
            # if it is NOT, prevent that move from happening (the move is illegal)
            if self.grid.get(next_next_pos) != Cell.empty:
                return
        elif self.grid.get(next_pos) == Cell.wall:
            return

        self.dir = Dir.up
        self.next_pos = (self.rect.x, next_y)
        self.velocity.y = -PLAYER_SPEED
        self.moves += 1
        self.grid[self.next_pos] = Cell.player
        self.grid[(self.rect.x, self.rect.y)] = Cell.empty

    def move_down(self):
        if self.dir:
            return

        next_y = self.rect.y + BLOCK_SIDE
        if next_y > SCREEN_LIMIT:
            return

        next_pos = (self.rect.x, next_y)
        if self.grid.get(next_pos) == Cell.block:
            next_next_pos = (self.rect.x, next_y + BLOCK_SIDE)
            if next_next_pos[1] > SCREEN_LIMIT:
                return
            if self.grid.get(next_next_pos) != Cell.empty:
                return
        elif self.grid.get(next_pos) == Cell.wall:
            return

        self.dir = Dir.down
        self.next_pos = (self.rect.x, next_y)
        self.velocity.y = PLAYER_SPEED
        self.moves += 1
        self.grid[self.next_pos] = Cell.player
        self.grid[(self.rect.x, self.rect.y)] = Cell.empty

    def move_left(self):
        if self.dir:
            return

        next_x = self.rect.x - BLOCK_SIDE
        if next_x < 0:
            return

        next_pos = (next_x, self.rect.y)
        if self.grid.get(next_pos) == Cell.block:
            next_next_pos = (next_x - BLOCK_SIDE, self.rect.y)
            if next_next_pos[0] < 0:
                return
            elif self.grid.get(next_next_pos) != Cell.empty:
                return
        elif self.grid.get(next_pos) == Cell.wall:
            return

        self.dir = Dir.left
        self.next_pos = (next_x, self.rect.y)
        self.velocity.x = -PLAYER_SPEED
        self.moves += 1
        self.grid[self.next_pos] = Cell.player
        self.grid[(self.rect.x, self.rect.y)] = Cell.empty

    def move_right(self):
        if self.dir:
            return

        next_x = self.rect.x + BLOCK_SIDE
        if next_x > SCREEN_LIMIT:
            return

        next_pos = (next_x, self.rect.y)
        if self.grid.get(next_pos) == Cell.block:
            next_next_pos = (next_pos[0] + BLOCK_SIDE, self.rect.y)
            if next_next_pos[0] > SCREEN_LIMIT:
                return
            elif self.grid.get(next_next_pos) != Cell.empty:
                return
        elif self.grid.get(next_pos) == Cell.wall:
            return

        self.dir = Dir.right
        self.next_pos = (next_x, self.rect.y)
        self.velocity.x = PLAYER_SPEED
        self.moves += 1
        self.grid[self.next_pos] = Cell.player
        self.grid[(self.rect.x, self.rect.y)] = Cell.empty

    def __init__(self, pos, grid):
        super().__init__()
        self.grid = grid
        self.grid[pos] = Cell.player
        self.moves = 0
        self.dir = None
        self.next_pos = None
        self.side = BLOCK_SIDE

        dim = (self.side, self.side)

        self.image = pygame.Surface(dim)

        # this sets the background of the sprite
        # to transparent
        self.image.fill(Color.black)
        self.image.set_colorkey(Color.black)

        rect = (0, 0, self.side, self.side)
        pygame.draw.rect(self.image, Color.player, rect)

        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = pos

        self.velocity = Velocity(0, 0)
