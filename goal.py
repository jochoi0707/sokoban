import pygame
from colors import Color
from constants import BLOCK_SIDE
from cell_types import Cell


class Goal(pygame.sprite.Sprite):
    def __init__(self, pos, grid):
        super().__init__()
        grid[pos] = Cell.empty
        dim = (BLOCK_SIDE, BLOCK_SIDE)
        self.image = pygame.Surface(dim)
        self.image.fill(Color.black)
        self.image.set_colorkey(Color.black)
        rect = (0, 0, BLOCK_SIDE, BLOCK_SIDE)
        pygame.draw.rect(self.image, Color.goal, rect)
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = pos
